##Atlassian Connect [JIRA Activity Tutorial](https://developer.atlassian.com/static/connect/docs/guides/project-activity-tutorial.html) Source Code

You can use this repository to check your work for the [JIRA Activity Tutorial](https://developer.atlassian.com/static/connect/docs/guides/project-activity-tutorial.html). This tutorial shows you how to build a static Connect add-on that displays your JIRA projects 
in a table, accessible via an _Activity_ link in the header. 

This add-on uses the [JIRA REST API](https://docs.atlassian.com/jira/REST/latest/) 
to get information about projects in your instance. It uses the [Node.js](http://nodejs.org/) 
framework and [Atlassian Connect Express (ACE)](https://bitbucket.org/atlassian/atlassian-connect-express/) to interface with JIRA. This add-on displays JIRA projects in a table using [D3.js](http://d3js.org/).

Here's an example how this add-on might look (projects may vary, of course):

![JIRA-project-activity.png](https://bitbucket.org/repo/Roe447/images/1488343659-JIRA-project-activity.png)

##[Check out the tutorial >>](https://developer.atlassian.com/static/connect/docs/guides/project-activity-tutorial.html)